# Bible Syntax Highlighting

Notepad++ syntax highlighting for (German) bible texts to mark important words, etc.
This was build to help to structure and study bible texts.

## Highlighting

### Keywords

- Words referring to God, Jesus Christ or the Holy Spirit will be coloured $`\textcolor{red}{\textbf{red}}`$.
- Words referring to Satan will be coloured $`\textcolor{purple}{\textbf{purple}}`$.
- Words referring to fruits of the Holy Spirit will be coloured $`\textcolor{orange}{\textbf{orange}}`$.
- Words referring to bad habits will be coloured $`\textcolor{magenta}{\textbf{light purple}}`$.
- Words referring to commands and law will be coloured $`\textcolor{brown}{\underline{\textbf{brown}}}`$.
- Words used to structure text will be coloured $`\textcolor{blue}{\textbf{blue}}`$.

### Blocks

- Text blocks may be outlined using brackets `(` `)` or `\b` and `\e`.

### Comments

- Inline comments may be put with `<` and `>` and will be coloured $`\textcolor{gray}{\textbf{gray}}`$.
- Line comments may be put after `#` and will be coloured also $`\textcolor{gray}{\textbf{gray}}`$.

### Emphasize words or passages

- Anything with `*` and `*` will be coloured $`\colorbox{yellow}{\textcolor{gray}{\textbf{yellow}}}`$. This applies to any text including keywords or comments.

## Download

Download the latest release here: [notepad++.zip](https://gitlab.com/imblickfeld/bible-syntax-highlighting/-/raw/master/release/notepad++.zip?inline=false)

## Install

After download unpack the ZIP file and simply copy `bible-udl.de.xml` to `%APPDATA%\Notepad++\userDefineLangs\` in Windows.

After install you may open `1Mo01.elb.bibel` with Notepad++ and you should see it coloured:

![Example screenshot](syntax/example.png "Example screenshot")
