@echo off
set CURDIR=%CD%
rem set TARGETDIR=..\release
set OBJDIR=bin\Release
set PROJECTNAME=bible-convert

if not defined VS_PATH set "VS_PATH=c:\Programs\Compiler\VS2019\Common7\"
call "%VS_PATH%Tools\VsDevCmd.bat"
rem VsDevCmd.bat scheint den aktuellen Pfad zu �ndern...
cd /d "%CURDIR%"

del /s /q /f %OBJDIR%\*.*
rem del /s /q /f %TARGETDIR%\*.*

msbuild %PROJECTNAME%.csproj /p:Configuration=Release /p:Platform="Any CPU" /p:OutputPath=%OBJDIR% || goto Error

rem mkdir %TARGETDIR%
rem cd %OBJDIR%
rem 7za a -tzip -r -x!*.pdb -x!*.xml ..\..\%TARGETDIR%\%PROJECTNAME%.zip .
rem cd %CURDIR%

rem del /s /q %OBJDIR%\*.*

echo Done.
goto :EOF

:Error
echo Failed.