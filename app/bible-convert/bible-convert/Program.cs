﻿using imBlickFeld.BibleConvert.Modules;
using imBlickFeld.CommonBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace imBlickFeld.BibleConvert
{
    class Program
    {
        static int Main(string[] args) {
#if DEBUG
            try {
#endif
                var cfg = Config.I;
                try {
                    cfg.Init(Util.ParseParams(args));
                } catch (Exception e) {
                    ShowHelp(e);
                    return 1;
                }

                if (cfg.ShowHelp) {
                    ShowHelp();
                    return 0;
                }

                try {
                    if (cfg.ToC)
                        new ToCCreator().Run();
                } catch (ConversionException e) {
                    string filename = cfg.InputFilename;
                    if (e.IncludeFilename && filename != null)
                        Console.Error.Write(filename + ": ");
                    Console.Error.WriteLine(e.Message);
                    return 2;
                } catch (Exception e) {
                    Console.Error.WriteLine(e);
                    return 99;
                }

                return 0;
#if DEBUG
            } finally {
                Console.WriteLine("Press any key...");
                Console.ReadKey();
            }
#endif
        }

        private static void ShowHelp(Exception e = null) {
            Console.WriteLine("This is bible-convert.");
            if (e != null)
                Console.WriteLine("\n" + e.Message);
            Console.WriteLine();
            Console.WriteLine($@"Usage {System.AppDomain.CurrentDomain.FriendlyName} OPTIONS
while OPTIONS are:

        --toc                   Create a table of contents from bible input \b-\e blocks.
        --output-format FMT     Output format. Valid values for FMT are ""Text"" and ""LaTeX"".
                                Default is ""Text"".
        
    -i, --input-file FILE       The .bible input FILE. If missing stdin is used.
    -o, --output-file FILE      The output FILE. The file format depends on --output-format. 
                                If missing stdout is used.

    -t, --title                 Include <title> comments (default).
    -T, --no-title              Do not include <title> comments.

    -l, --translation           Include <translation> comments (default).
    -L, --no-translation        Do not include <translation> comments.

    -v, --verbose               Verbose output on stderr.

    -h, --help                  Print this help and exit.
");
        }
    }
}
