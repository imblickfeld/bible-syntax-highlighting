﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace imBlickFeld.BibleConvert.Modules
{
    class ToCCreator : Module
    {
        private const string VERSE_PATTERN = @"^(\d+)\W";

        public override void Run() {
            Log(LogSeverity.INFO, "Creating table of contents...");
            using (var reader = new StreamReader(_cfg.InputStream))
            using (var writer = new StreamWriter(_cfg.OutputStream)) {
                Stack<Block> stack = new Stack<Block>();
                List<Block> blocks = new List<Block>();
                Block innerBlock = null;
                string line;
                int lineNo = 0;
                while ((line = reader.ReadLine()) != null) {
                    ++lineNo;
                    if (string.IsNullOrWhiteSpace(line))
                        continue;

                    // Read verse number
                    int verseNumber;
                    if (innerBlock != null && (verseNumber = ReadVerseNumber(ref line)) != 0) {
                        if (innerBlock.FirstVerse == 0) {
                            foreach (var block in stack.Where(x => x.FirstVerse == 0))
                                block.FirstVerse = verseNumber;
                        }  else
                            innerBlock.LastVerse = verseNumber;
                    }

                    line = line.Trim();

                    // Read starting line comments
                    if (line.Length > 1 && line[0] == '#') {
                        string comment = line.Substring(1).TrimStart();
                        if (comment.Length > 0)
                            blocks.Add(new Comment(comment));
                    }                            

                    // Read \b | \e
                    if (line.Length < 2)
                        continue;
                    string keyword = line.Substring(0, 2);
                    if (keyword == "\\b") {
                        Log(LogSeverity.DEBUG, lineNo + ":" + line, true);
                        string title = line.Substring(line.IndexOf('#') + 1).TrimStart();
                        innerBlock = new Block(stack.Count, title);
                        stack.Push(innerBlock);
                    } else if (keyword == "\\e") {
                        Log(LogSeverity.DEBUG, lineNo + ":" + line, true);
                        try {
                            var block = stack.Pop();
                            blocks.Add(block);
                            if (stack.Count > 0) {
                                innerBlock = stack.Peek();
                                innerBlock.LastVerse = block.LastVerse;
                            }
                        } catch (InvalidOperationException e) {
                            throw new ConversionException($"\\e before \\b found at line {lineNo}", true, e);
                        }
                    }
                }
                Block lastBlock = null;
                foreach (var block in blocks.OrderBy(x => x.FirstVerse).ThenBy(x => x.Index).ThenBy(x => x.LastVerse)) {
                    WriteContentsLine(writer, block, lastBlock);
                    lastBlock = block;
                }
            }
        }

        private static int ReadVerseNumber(ref string line) {
            var match = Regex.Match(line, VERSE_PATTERN);
            if (match.Success) {
                line = line.Substring(match.Length);
                return int.Parse(match.Value);
            }
            if (line[0] == ' ' || line[0] == '\t')
                line = line.Substring(1);
            return 0;
        }

        private void WriteContentsLine(StreamWriter writer, Block block, Block lastBlock) {
            switch (_cfg.OutputFormat) {
                case OutputFormat.Text:
                    if (lastBlock is Comment && !(block is Comment))
                        writer.WriteLine();  // Empty line after line comments.
                    writer.Write(new string('\t', block.Index));
                    writer.Write(block.Title);
                    if (block.FirstVerse > 0)
                        writer.Write($" ({block.FirstVerse} – {block.LastVerse})");
                    writer.WriteLine();
                    break;
                case OutputFormat.LaTeX:
                    var comment = block as Comment;
                    if (comment != null) {
                        if (comment.Type == CommentType.Title && _cfg.IncludeTitle) {
                            writer.WriteLine($@"\subsection{{{block.Title}}}");
                            writer.WriteLine();
                        } else if (comment.Type == CommentType.Translation && _cfg.IncludeTranslation) {
                            writer.WriteLine($@"\emph{{{block.Title}}}");
                            writer.WriteLine();
                        }
                        break;
                    }
                    writer.WriteLine(
                        @"\contentsline{" + GetTeXSection(block.Index) + "}{" 
                        + EscapeTeX(block.Title) + "}{" 
                        + block.FirstVerse + (block.LastVerse > block.FirstVerse ? " -- " + block.LastVerse : "") + "}{}");
                    break;
                default:
                    throw new ConversionException($"Unknown output format: {_cfg.OutputFormat}; valid formats are:" + 
                        Enum.GetNames(typeof(OutputFormat))
                            .Select(x => "- " + x)
                            .ToString("\n"));
            }
        }

        private string GetTeXSection(int index) {
            switch (index) {
                case 0: return "section";
                case 1: return "subsection";
                case 2: return "subsubsection";
                case 3: return "paragraph";
                default: return "subparagraph";
            }
        }

        private static readonly string[] texSrc = { "\\", " \"", "\"", "/", "#", "$", "%", "&", "^", "_", "{", "}", "~", "<", ">", "|", "–", " - ", "/\"\"/\"\"" };
        private static readonly string[] texTrg = { "\\textbackslash ", " \\glqq ", "\\grqq ", "/\"\"", "\\#", "\\$", "\\%", "\\&", "\\textasciicircum ", "\\_", "\\{", "\\}", "\\textasciitilde ", "\\textless ", "\\textgreater ", "\\textbar ", "--", " -- ", "//\"\"" };

        private string EscapeTeX(string text) {
            if (string.IsNullOrEmpty(text))
                return "";
            for (int i = 0; i < texSrc.Length; ++i) {
                text = text.Replace(texSrc[i], texTrg[i]);
            }
            return text;
        }

        [DebuggerDisplay("{Index}: {Title}")]
        private class Block
        {
            public readonly int Index;
            public readonly string Title;
            public int FirstVerse;
            public int LastVerse;

            public Block(int index, string title) {
                Index = index;
                Title = title;
            }
        }

        private class Comment : Block
        {
            public readonly CommentType Type;

            public Comment(string comment) : base(0, ExtractCommentType(comment, out CommentType type)) {
                Type = type;
            }

            private static string ExtractCommentType(string comment, out CommentType type) {
                if (comment.StartsWith("<title>", StringComparison.InvariantCulture)) {
                    type = CommentType.Title;
                    return comment.Substring(7).TrimStart();
                }
                if (comment.StartsWith("<translation>", StringComparison.InvariantCulture)) {
                    type = CommentType.Translation;
                    return comment.Substring(13).TrimStart();
                }
                type = CommentType.Comment;
                return comment;
            }
        }

        private enum CommentType
        {
            Comment,
            Title,
            Translation
        }
    }
}
