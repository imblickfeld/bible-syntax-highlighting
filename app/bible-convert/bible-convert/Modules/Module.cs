﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace imBlickFeld.BibleConvert.Modules
{
    abstract class Module
    {
        protected Config _cfg = Config.I;

        /// <exception cref="InvalidOperationException">May be thrown in case of (expected) error.</exception>
        public abstract void Run();

        protected void Log(LogSeverity severity, string msg, bool includeFilename = false) {
            if (severity > LogSeverity.INFO && !_cfg.Verbose)
                return;
            if (severity >= LogSeverity.INFO && _cfg.StdOut && !_cfg.Verbose)
                return;
            var writer = severity >= LogSeverity.INFO && !_cfg.Verbose ? Console.Out : Console.Error;
            if (severity != LogSeverity.INFO)
                writer.Write("[" + severity + "] ");
            if (includeFilename && _cfg.InputFilename != null)
                writer.Write(_cfg.InputFilename + ": ");
            writer.WriteLine(msg);
        }

        protected enum LogSeverity
        {
            ERROR = 0,
            WARNING = 1,
            INFO = 2,
            DEBUG = 3
        }
    }
}
