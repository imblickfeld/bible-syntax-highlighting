﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace imBlickFeld.BibleConvert
{
    class Config
    {
        public static Config I = new Config();

        private Config() {
            OutputFormat = OutputFormat.Text;
        }

        public string InputFilename { get; private set; }
        public Stream InputStream { get; private set; }
        public Stream OutputStream { get; private set; }
        public bool StdOut { get; private set; }

        public OutputFormat OutputFormat { get; private set; }

        /// <summary>Create ToC (Table of Contents)</summary>
        public bool ToC { get; private set; }

        public bool IncludeTitle { get; private set; }
        public bool IncludeTranslation { get; private set; }

        public bool Verbose { get; private set; }

        public bool ShowHelp { get; private set; }

        public void Init(IDictionary<string, string> parameters) {
            try {
                ShowHelp = parameters.ContainsKey("h") || parameters.ContainsKey("help");
                if (ShowHelp)
                    return;

                ToC = parameters.ContainsKey("toc");

                if (parameters.TryGetValue("output-format", out string of)) {
                    OutputFormat = (OutputFormat) Enum.Parse(typeof(OutputFormat), of);
                }

                IncludeTitle = true;
                if (parameters.ContainsKey("T") || parameters.ContainsKey("no-title"))
                    IncludeTitle = false;

                IncludeTranslation = true;
                if (parameters.ContainsKey("L") || parameters.ContainsKey("no-translation"))
                    IncludeTranslation = false;

                Verbose = parameters.ContainsKey("v") || parameters.ContainsKey("verbose");

                if (!(ToC || ShowHelp))
                    throw new ArgumentException("What shall I do?");

                string filename = parameters.GetValueOrDefault("i") ?? parameters.GetValueOrDefault("input-file");
                InputFilename = filename;
                InputStream = filename != null ? new FileStream(filename, FileMode.Open, FileAccess.Read) : Console.OpenStandardInput();

                filename = parameters.GetValueOrDefault("o") ?? parameters.GetValueOrDefault("output-file");
                OutputStream = filename != null ? new FileStream(filename, FileMode.Create, FileAccess.Write) : Console.OpenStandardOutput();
                StdOut = filename == null;
            } catch (Exception) {
                if (InputStream != null)
                    InputStream.Close();
                if (OutputStream != null)
                    OutputStream.Close();
                throw;
            }
        }
    }

    enum LogSeverity
    {
        Error = 0,
        Warning = 1,
        Info = 2,
        Debug = 3
    }

    enum OutputFormat
    {
        Text,
        LaTeX
    }
}
