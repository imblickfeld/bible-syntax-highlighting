﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace imBlickFeld.BibleConvert
{
    public class ConversionException : Exception
    {
        public readonly bool IncludeFilename;

        public ConversionException(string message, bool includeFilename = false) : base(message) {
            IncludeFilename = includeFilename;
        }

        public ConversionException(string message, bool includeFilename, Exception innerException) : base(message, innerException) {
            IncludeFilename = includeFilename;
        }
    }
}
