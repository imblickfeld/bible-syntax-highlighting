all: release

RELEASE_DIR=release

clean:
	-rm -rf $(RELEASE_DIR)
	mkdir -p $(RELEASE_DIR)

release-app:
	make -C app/bible-convert/
	
release-bibleserver:
	cd bibleserver; 7za a -tzip -r ../$(RELEASE_DIR)/bibleserver.zip

release-npp:
	cd syntax; 7za a -tzip -r ../$(RELEASE_DIR)/notepad++.zip

release: clean release-app release-bibleserver release-npp