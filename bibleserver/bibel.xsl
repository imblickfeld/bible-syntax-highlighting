<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

	<xsl:output method="text"/>

    <xsl:template match="@*|node()">
        <xsl:apply-templates select="@*|node()"/>
    </xsl:template>

	<xsl:template match="//main//article">
		<xsl:text>&#9;# &lt;title&gt; </xsl:text>
		<xsl:value-of select="header/h1/text()"/>
		<xsl:text>&#10;&#9;# &lt;translation&gt; </xsl:text>
		<xsl:value-of select="header/h2/text()"/>
		<xsl:text>&#10;&#10;</xsl:text>
		<xsl:for-each select="h3|span">
			<xsl:if test="name(.)='h3'">
				<xsl:text>&#10;&#9;# </xsl:text>
				<xsl:value-of select="span/text()"/>
				<xsl:text>&#10;</xsl:text>
			</xsl:if>
			<xsl:if test="name(.)='span'">
				<xsl:value-of select="span/span[@class='verse-number__group']/span"/>
				<xsl:text>&#9;</xsl:text>
				<xsl:value-of select="span/span[@class='verse-content--hover']"/>
				<xsl:if test="span/span[@class='verse-references']">
					<xsl:text>&#10;&#9;&lt;&lt;ref: # </xsl:text>
					<xsl:for-each select="span/span[@class='verse-references']/span/span/a">
						<xsl:value-of select="text()"/>
						<xsl:if test="position() != last()">
							 <xsl:text>; </xsl:text>
						</xsl:if>
					</xsl:for-each>
					<xsl:text> #&gt;&gt;</xsl:text>
				</xsl:if>
			</xsl:if>
			<xsl:text>&#10;</xsl:text>
		</xsl:for-each>
	</xsl:template>

</xsl:stylesheet>